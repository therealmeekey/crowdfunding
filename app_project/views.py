from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, DetailView, UpdateView

from app_comments.forms import CommentForm
from app_comments.models import Comment
from app_project.models import Project, Payment, Remuneration, News
from app_project.forms import ProjectForm, RemunerationFormSet, PaymentForm, NewsForm
from app_category.models import Category
from app_region.models import Region, City


class ProjectListView(ListView):
    model = Project
    template_name = 'app_project/project_list.html'
    paginate_by = 21

    def get_queryset(self):
        qs = Project.objects.published()
        if self.request.GET.get('categories') and int(self.request.GET.get('categories')) != 0:
            qs = qs.filter(category_id__in=[int(i) for i in self.request.GET.getlist('categories')])
        if self.request.GET.get('title'):
            qs = qs.filter(title__icontains=self.request.GET.get('title'))
        if self.request.GET.get('city') and int(self.request.GET.get('city')) != 0:
            qs = qs.filter(city_id__in=[int(i) for i in self.request.GET.getlist('city')])
        if self.request.GET.get('region') and int(self.request.GET.get('region')) != 0:
            qs = qs.filter(region_id__in=[int(i) for i in self.request.GET.getlist('region')])
        return qs

    def get_context_data(self, **kwargs):
        context = super(ProjectListView, self).get_context_data(**kwargs)
        context.update({
            'categories': Category.objects.all(),
            'regions': Region.objects.all(),
            'citys': City.objects.all()
        })
        print(self.request.GET)
        project_list = []
        if self.request.GET.get('categories'):
            for category in self.request.GET.getlist('categories'):
                print(category)
                project_list.append(Project.objects.published().filter(category_id=int(category)))
                context.update({
                    'categories': Category.objects.filter(
                        id__in=[int(i) for i in self.request.GET.getlist('categories')]),
                })
        if self.request.GET.get('city'):
            for city in self.request.GET.getlist('city'):
                project_list.append(Project.objects.published().filter(city_id=int(city)))
                context.update({
                    'city': City.objects.filter(id__in=[int(i) for i in self.request.GET.getlist('city')]),
                })
        if self.request.GET.get('region'):
            for region in self.request.GET.getlist('region'):
                project_list.append(Project.objects.published().filter(region_id=int(region)))
                context.update({
                    'region': Region.objects.filter(id__in=[int(i) for i in self.request.GET.getlist('region')]),
                })
        if self.request.GET.get('title'):
            context.update({
                'title': self.request.GET.get('title')
            })
        if self.request.META['QUERY_STRING']:
            self.request.session['project_filtered_list'] = '%s?%s' % (
                self.request.path, self.request.META['QUERY_STRING'])
        else:
            self.request.session['project_filtered_list'] = reverse('app_project:list')
        context.update({
            'projects': project_list,
        })
        return context


class ProjectDetailView(DetailView):
    model = Project
    template_name = 'app_project/project_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        context.update({
            'comment_form': CommentForm(),
            'payment_form': PaymentForm(),
            'news_form': NewsForm(),
            'comments': Comment.objects.filter(project=context['object']).order_by('id'),
            'payments': Payment.objects.filter(project_payment=context['object']),
            'news': News.objects.filter(project=context['object']).order_by('id'),
            'remunerations': Remuneration.objects.filter(project=context['object'], disable=False),
        })
        return context


class ProjectCreateView(CreateView):
    model = Project
    template_name = 'app_project/project_form.html'
    form_class = ProjectForm
    object = None

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        remuneration_form = RemunerationFormSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  remuneration_form=remuneration_form,
                                  )
        )

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        remuneration_form = RemunerationFormSet(self.request.POST, self.request.FILES)
        if form.is_valid() and remuneration_form.is_valid():
            return self.form_valid(form, remuneration_form)
        else:
            return self.form_invalid(form, remuneration_form)

    def form_valid(self, form, remuneration_form):
        try:
            self.object = form.save(commit=False)
            self.object.author = self.request.user
            self.object.save()
        except Exception:
            pass
        remunerations = remuneration_form.save(commit=False)
        for obj in remunerations:
            obj.delete()
        for rm in remunerations:
            rm.project = self.object
            rm.save()
        return HttpResponseRedirect(reverse('app_project:detail', args=self.object.slug))

    def form_invalid(self, form, remuneration_form):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  remuneration_form=remuneration_form
                                  )
        )


class ProjectUpdateView(CreateView):
    model = Project
    template_name = 'app_project/project_form.html'
    form_class = ProjectForm
    object = None

    def get_object(self, queryset=None):
        self.object = super(ProjectUpdateView, self).get_object()
        return self.object

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        remuneration_form = RemunerationFormSet(instance=self.object)
        return self.render_to_response(
            self.get_context_data(form=ProjectForm(instance=self.object),
                                  remuneration_form=remuneration_form,
                                  )
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = ProjectForm(data=self.request.POST, instance=self.object)
        remuneration_form = RemunerationFormSet(self.request.POST, self.request.FILES, instance=self.object)
        if form.is_valid() and remuneration_form.is_valid():
            return self.form_valid(form, remuneration_form)
        else:
            print('Проект ', form.errors, 'Вознаграждение', remuneration_form.errors)
            return self.form_invalid(form, remuneration_form)

    def form_valid(self, form, remuneration_form):
        try:
            self.object = form.save()
            self.object.author = self.request.user
            self.object.save()
        except Exception:
            pass
        remuneration = remuneration_form.save(commit=False)
        for rm in remuneration:
            rm.project = self.object
            rm.save()
        return HttpResponseRedirect(reverse('app_project:detail', kwargs={'slug': self.object.slug}))

    def form_invalid(self, form, remuneration_form):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  remuneration_form=remuneration_form
                                  )
        )


@csrf_exempt
def create_payment(request, pk, pk_r):
    context = {}
    project = Project.objects.get(id=pk)
    remuneration = None
    if pk_r != 0:
        remuneration = Remuneration.objects.get(id=pk_r)
    if request.method == "POST":
        print(request.POST['sum'])
        if remuneration.price == int(request.POST['sum']):
            payment_form = PaymentForm(request.POST)
            if payment_form.is_valid():
                payment = payment_form.save(commit=False)
                payment.author = request.user
                payment.project_payment = project
                payment.save()
                if remuneration:
                    payment.remuneration = remuneration
                    payment.save()
                remuneration.holders.add(request.user)
                remuneration.save()
                return redirect('app_project:detail', slug=project.slug)
            else:
                context.update({
                    'error': 'Поля заполнены не правильно'
                })
        else:
            context.update({
                'error': 'Суммы не совпадают'
            })
    else:
        payment_form = PaymentForm()
    return redirect('app_project:detail', slug=project.slug)


@csrf_exempt
def create_news(request, pk):
    project = Project.objects.get(id=pk)
    if request.method == "POST":
        news_form = NewsForm(request.POST)
        if news_form.is_valid():
            news = news_form.save(commit=False)
            news.author = request.user
            news.project = project
            news.save()
            return redirect('app_project:detail', slug=project.slug)
        else:
            print(news_form.errors)
    else:
        news_form = NewsForm()
    return redirect('app_project:detail', slug=project.slug)
