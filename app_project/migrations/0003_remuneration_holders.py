# Generated by Django 3.0.5 on 2020-04-15 11:48

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app_project', '0002_auto_20200414_2338'),
    ]

    operations = [
        migrations.AddField(
            model_name='remuneration',
            name='holders',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='Обладатели'),
        ),
    ]
