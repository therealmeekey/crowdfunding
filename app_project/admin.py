from django.contrib import admin
from app_project.models import Project, Remuneration, Payment, News
from django.contrib.auth.models import Group

PROJECT_FIELDSETS = (
    ('Статус', {'fields': ('status', 'state')}),
    ('Обязательные поля',
     {'fields': (
         'author', 'title', 'pre_title', 'youtube', 'description', 'image', 'estimated_amount', 'category', 'region',
         'city')}),
    ('Необязательные поля', {'fields': ('important', 'published_date', 'date_end', 'slug')}),
)


class RemunerationInline(admin.TabularInline):
    model = Remuneration
    insert_after = 'slug'
    extra = 1


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    fieldsets = PROJECT_FIELDSETS
    list_display = ('title', 'author', 'category', 'status')
    list_filter = ('status', 'category')
    search_fields = ('title',)
    date_hierarchy = 'published_date'
    ordering = ['status', '-published_date']
    readonly_fields = ['slug', 'published_date', 'date_end']
    inlines = [RemunerationInline]



@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'project', 'created')


admin.site.register(Payment)

admin.site.site_header = 'Сайт Администратора'
admin.site.site_title = 'Сайт Администратора'
admin.site.index_title = 'Администратор crowdfunding'
admin.site.site_url = '/'
admin.site.unregister(Group)
