from django import forms

from app_project.models import Project, Remuneration, Payment, News
from app_category.models import Category
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from django.forms.models import inlineformset_factory

from app_region.models import Region, City


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['title', 'description', 'image', 'estimated_amount', 'category', 'pre_title', 'youtube', 'region',
                  'city']
        widgets = {
            'title': forms.TextInput(),
            'description': CKEditorUploadingWidget(),
            'image': forms.ClearableFileInput(),
            'estimated_amount': forms.NumberInput(),
            'category': forms.TextInput(),
            'region': forms.Select(),
            'city': forms.Select(),
            'pre_title': forms.TextInput(),
            'youtube': forms.TextInput(),

        }

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['category'].choices = [e for e in Category.objects.all()]
        self.fields['region'].choices = [e for e in Region.objects.all()]
        self.fields['city'].choices = [e for e in City.objects.all()]


RemunerationFormSet = inlineformset_factory(Project,
                                            Remuneration,
                                            fields=['title', 'image', 'description', 'price', 'disable'],
                                            widgets={'title': forms.TextInput(),
                                                     'description': forms.Textarea(
                                                         attrs={'class': 'materialize-textarea'}),
                                                     'image': forms.ClearableFileInput(),
                                                     'price': forms.NumberInput(),
                                                     'disable': forms.CheckboxInput(),
                                                     },
                                            can_delete=True,
                                            extra=1)


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ['sum', 'remuneration']
        widgets = {
            'sum': forms.NumberInput(),
            'remuneration': forms.Select(),
        }

    def __init__(self, *args, **kwargs):
        super(PaymentForm, self).__init__(*args, **kwargs)
        self.fields['remuneration'].choices = [e for e in Remuneration.objects.all()]


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ['title', 'description']
        widgets = {
            'title': forms.TextInput(),
            'description': CKEditorUploadingWidget(),
        }
