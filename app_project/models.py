from django.db import models
from django.db.models import Sum
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
from pytils.translit import slugify
from djchoices import ChoiceItem, DjangoChoices
from app_region.models import Region, City
from app_accounts.models import User
from app_category.models import Category
from crowfounding.validators import YoutubeValidator
import datetime


class ProjectManager(models.Manager):
    def published(self):
        return self.filter(status=ProjectStatus.published)


class ProjectStatus(DjangoChoices):
    published = ChoiceItem('published', 'Опубликован')
    pending = ChoiceItem('pending', 'На рассмотрении')
    unpublished = ChoiceItem('unpublished', 'Отклонен')


class ProjectState(DjangoChoices):
    gathering = ChoiceItem('published', 'Идет сбор')
    finished = ChoiceItem('pending', 'Закончен')


class Project(models.Model):
    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'
        ordering = ['-published_date']

    author = models.ForeignKey(User, related_name='author', verbose_name='Автор', null=True, on_delete=models.PROTECT)
    title = models.CharField('Наименование', max_length=255)
    description = RichTextUploadingField('Описание')
    image = models.ImageField('Изображение', upload_to='project_image')
    published_date = models.DateField(blank=True, null=True, default=timezone.now, verbose_name='Дата публикации')
    estimated_amount = models.PositiveIntegerField('Планируемая сумма')
    date_end = models.DateField(blank=True, null=True, verbose_name='Дата окончания')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    important = models.BooleanField('Важность', default=False)
    status = models.CharField('Статус публикации', max_length=50, choices=ProjectStatus.choices,
                              default=ProjectStatus.pending)
    pre_title = models.CharField('Подзаголовок', max_length=255)
    state = models.CharField('Статус сбора средств', max_length=50, choices=ProjectState.choices,
                             default=ProjectState.gathering)
    youtube = models.CharField('Ссылка на Youtube видео', max_length=255, validators=[YoutubeValidator()], null=True,
                               blank=True)
    region = models.ForeignKey(Region, on_delete=models.PROTECT, verbose_name='Регион')
    city = models.ForeignKey(City, on_delete=models.PROTECT, verbose_name='Город')
    slug = models.SlugField(allow_unicode=True, unique=True,
                            max_length=255, null=True, blank=True,
                            help_text='Строковый идентификатор сущности. Не может быть числом. '
                                      'После создания в начало будет дописан номер сущности.')
    objects = ProjectManager()

    def __str__(self):
        return self.title

    def count_payment(self):
        """
        Показывает кол. поступлений
        """
        return self.payment_set.all().count()

    def left_days(self):
        """
        Показывает сколько дней осталось
        """
        return int((self.date_end - self.published_date).days)

    def current_payment(self):
        """
        Показывает текущую сумму поступлений
        """
        payments = self.payment_set.aggregate(Sum('sum'))['sum__sum'] or 0
        return int(payments)

    def percent_payment(self):
        """
        Показывает кол. поступлений в процентах
        """
        return int(int(self.current_payment()) * 100 / self.estimated_amount)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        self.published_date = timezone.now()
        self.date_end = self.published_date + datetime.timedelta(days=30)
        super(Project, self).save(*args, **kwargs)


class Remuneration(models.Model):
    class Meta:
        verbose_name = 'Вознаграждение'
        verbose_name_plural = 'Вознаграждения'
        ordering = ['-price']

    project = models.ForeignKey('Project', null=True, verbose_name='Проект', on_delete=models.CASCADE)
    title = models.CharField('Наименование', max_length=255)
    image = models.ImageField('Изображение', upload_to='remuneration_image')
    description = models.TextField('Описание')
    disable = models.BooleanField('Заблокировать вознаграждение?', default=False)
    price = models.PositiveIntegerField('Cумма')
    holders = models.ManyToManyField(User, blank=True, verbose_name='Владельцы')

    def __str__(self):
        return self.title


class Payment(models.Model):
    class Meta:
        verbose_name = 'Поступление'
        verbose_name_plural = 'Поступления'
        ordering = ['-created']

    project_payment = models.ForeignKey('Project', on_delete=models.PROTECT, verbose_name='Проект')
    author = models.ForeignKey(User, related_name='author_payment', verbose_name='Автор поддержки', null=True,
                               on_delete=models.CASCADE)
    sum = models.DecimalField('Сумма', max_digits=11, decimal_places=2)
    remuneration = models.ForeignKey(Remuneration, on_delete=models.PROTECT, null=True, blank=True,
                                     verbose_name='Вознаграждение')
    created = models.DateField('Дата создания')

    def __str__(self):
        return 'Поступление на сумму %s руб. Дата: %s' % (self.sum, self.created)

    def save(self, *args, **kwargs):
        self.created = timezone.now()
        super(Payment, self).save(*args, **kwargs)


class News(models.Model):
    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-id']

    project = models.ForeignKey('Project', null=True, verbose_name='Проект', on_delete=models.PROTECT)
    author = models.ForeignKey(User, related_name='author_news', verbose_name='Автор', null=True,
                               on_delete=models.PROTECT)
    title = models.CharField('Наименование', max_length=255)
    description = RichTextUploadingField('Описание')
    created = models.DateField('Дата создания')

    def __str__(self):
        return '%s - %s - %s' % (self.title, self.project, self.created)

    def save(self, *args, **kwargs):
        self.created = timezone.now()
        super(News, self).save(*args, **kwargs)
