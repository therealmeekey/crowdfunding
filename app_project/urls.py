from django.contrib.auth.decorators import login_required
from django.urls import path
from app_project.views import *

app_name = 'app_project'

urlpatterns = (
    path('list/', ProjectListView.as_view(), name='list'),
    path('project_add/', login_required(ProjectCreateView.as_view()), name='create'),
    path('project_update/<str:slug>/', login_required(ProjectUpdateView.as_view()), name='update'),
    path('detail/<str:slug>/', ProjectDetailView.as_view(), name='detail'),
    path('create_payment/<int:pk>/<int:pk_r>/', login_required(create_payment), name='create_payment'),
    path('create_news/<int:pk>/', login_required(create_news), name='create_news'),
)
