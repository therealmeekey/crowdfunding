from django.contrib import admin
from app_category.models import Category
from django.http import HttpResponseRedirect
from django.urls import reverse
from app_category.views import create_category
from functools import update_wrapper


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'slug')
    readonly_fields = ['slug']

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        urlpatterns = super().get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name

        urls = [
                   path('create_category/', wrap(self.create_category), name='%s_%s_create_category' % info),
               ] + urlpatterns

        return urls

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        return super().changelist_view(request, extra_context=extra_context)

    def create_category(self, request):
        info = self.model._meta.app_label, self.model._meta.model_name
        create_category()
        redirect_url = reverse('admin:%s_%s_changelist' % info)
        return HttpResponseRedirect(redirect_url)
