from django.db import models
from pytils.translit import slugify


class Category(models.Model):
    class Meta:
        ordering = ['-id']
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    name = models.CharField('Название', max_length=255)
    image = models.ImageField('Обложка', upload_to='category_image', blank=True, null=True)
    slug = models.SlugField('Идентификатор', null=True, blank=True, default='')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)
