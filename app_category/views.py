import csv

import os
from django.http import HttpResponse
from app_category.models import Category

DATA_PATH = os.path.join(os.path.dirname(__file__), 'data')


def create_category():
    file = open(DATA_PATH + '/category.csv', 'r')
    data = csv.reader(file)
    i = 0
    for row in data:
        category, crcategory = Category.objects.get_or_create(name=row[0].replace(';', ''))
    return HttpResponse(status=200)
