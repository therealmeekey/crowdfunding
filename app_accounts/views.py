from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, UpdateView

from app_accounts.models import User
from app_project.models import Project, Payment, Remuneration
from app_accounts.forms import UserForm


@csrf_exempt
def user_register(request):
    context = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')

        if password and password2 and password == password2:
            user, ucr = User.objects.get_or_create(email=email)
            if ucr:
                user.set_password(password)
                user.first_name = request.POST.get('first_name')
                user.last_name = request.POST.get('last_name')
                user.save()
                user = authenticate(email=email, password=password)
                login(request, user)
                return HttpResponseRedirect(reverse('accounts:update', kwargs={'pk': user.pk}))
            else:
                context.update({'error': 'Аккаунт с таким email уже существует!'})
        else:
            context.update({'error': 'Пароли не совподают!'})
    return render(request, 'app_accounts/login.html', context)


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('landing:index'))


@csrf_exempt
def user_login(request):
    error = None
    if request.method == "POST":
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(email=email, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('landing:index'))
            else:
                error = 'Пользователь заблокирован'
        else:
            error = 'Вы ввели неверный e-mail или пароль'
    context = {'error': error}
    return render(request, 'app_accounts/login.html', context)


class UserDetailView(DetailView):
    model = User
    template_name = 'app_accounts/account_detail.html'

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        context.update({
            'created_projects': Project.objects.filter(author=context['object']),
            'payment_projects': Payment.objects.filter(author=context['object']),
            'remunerations': Remuneration.objects.filter(holders=context['object']),
        })
        return context


class UserUpdateView(UpdateView):
    model = User
    template_name = 'app_accounts/account_update.html'
    form_class = UserForm
    object = None

    def get_object(self, queryset=None):
        self.object = super(UserUpdateView, self).get_object()
        return self.object

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return self.render_to_response(
            self.get_context_data(form=UserForm(instance=self.object))
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = UserForm(self.request.POST, self.request.FILES, instance=self.object)
        if form.is_valid():
            return self.form_valid(form, )
        else:
            print('Проект ', form.errors)
            return self.form_invalid(form, )

    def form_valid(self, form):
        self.object = form.save()
        self.object.save()
        return HttpResponseRedirect(reverse('accounts:detail', args=(self.object.pk,)))

    def form_invalid(self, form):
        return self.render_to_response(
            self.get_context_data(form=form)
        )
