from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.conf import settings
from django.core.validators import EmailValidator
from djchoices import DjangoChoices, ChoiceItem
from phonenumber_field.modelfields import PhoneNumberField

from app_region.models import Region, City
from crowfounding.validators import VkValidator, FacebookValidator, YoutubeValidator, TwitterValidator, OkValidator, \
    InstagramValidator


class SocialLinksMixin(models.Model):
    vk = models.CharField('Вконтакте', max_length=255, null=True, blank=True,
                          validators=[VkValidator()])
    facebook = models.CharField('Facebook', max_length=255, null=True, blank=True,
                                validators=[FacebookValidator()])
    youtube = models.CharField('Youtube', max_length=255, null=True, blank=True,
                               validators=[YoutubeValidator()])
    twitter = models.CharField('Twitter', max_length=255, null=True, blank=True,
                               validators=[TwitterValidator()])
    ok = models.CharField('Одноклассники', max_length=255, null=True, blank=True,
                          validators=[OkValidator()])
    instagram = models.CharField('Instagram', max_length=255, null=True, blank=True,
                                 validators=[InstagramValidator()])

    class Meta:
        abstract = True


class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("Укажите номер телефона!")
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault('user_type', 1)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        if extra_fields.get('is_active') is not True:
            raise ValueError('Superuser must have is_active=True')
        return self.create_user(email, password, **extra_fields)


class UserType(DjangoChoices):
    admin = ChoiceItem(1, 'Администратор')
    moderator = ChoiceItem(2, 'Модератор')
    user = ChoiceItem(3, 'Пользователь')
    nko = ChoiceItem(4, 'НКО')
    gos = ChoiceItem(5, 'Государственное учреждение')


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    email = models.EmailField(validators=[EmailValidator], unique=True, verbose_name="Email")
    image = models.ImageField(verbose_name="Аватар", blank=True, null=True, upload_to='user_image')
    first_name = models.CharField(verbose_name="Имя", max_length=64, blank=True, null=True)
    middle_name = models.CharField(verbose_name="Отчество", max_length=64, blank=True, null=True)
    last_name = models.CharField(verbose_name="Фамилия", max_length=64, blank=True, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=settings.IS_ACTIVE)
    user_type = models.PositiveSmallIntegerField(verbose_name='Уровень доступа', default=UserType.user,
                                                 choices=UserType.choices, blank=True, null=True)
    region = models.ForeignKey(Region, on_delete=models.PROTECT, verbose_name='Регион', blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.PROTECT, verbose_name='Город', blank=True, null=True)
    bio = RichTextUploadingField('Краткая биография', blank=True, null=True, default='Заполните биографию')
    phone = PhoneNumberField('Номер телефона', blank=True, null=True)
    name_organization = models.CharField(verbose_name="Название организации", max_length=255, blank=True, null=True)
    date_joined = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        if self.first_name and self.middle_name and self.last_name:
            return '{} {} {}'.format(self.last_name, self.first_name, self.middle_name)
        elif self.first_name and self.last_name:
            return '{} {}'.format(self.last_name, self.first_name)
        elif self.first_name and self.middle_name:
            return '{} {}'.format(self.first_name, self.middle_name)
        else:
            return self.email
