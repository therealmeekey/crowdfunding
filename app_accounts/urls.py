from django.urls import path
from app_accounts.views import *
from django.contrib.auth.decorators import login_required

app_name = 'accounts'

urlpatterns = (
    path('login/', user_login, name='user_login'),
    path('logout/', user_logout, name='logout'),
    path('register/', user_register, name='register'),
    path('profile/<int:pk>/', UserDetailView.as_view(), name='detail'),
    path('profile/<int:pk>/update/', login_required(UserUpdateView.as_view()), name='update'),
)
