from django import forms

from app_accounts.models import User
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from app_region.models import Region, City


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'middle_name', 'bio', 'image', 'region', 'city', 'phone']
        widgets = {
            'first_name': forms.TextInput(),
            'last_name': forms.TextInput(),
            'middle_name': forms.TextInput(),
            'bio': CKEditorUploadingWidget(),
            'image': forms.ClearableFileInput(),
            'region': forms.Select(),
            'city': forms.Select(),
            'phone': forms.TextInput(),

        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['region'].choices = [e for e in Region.objects.all()]
        self.fields['city'].choices = [e for e in City.objects.all()]
