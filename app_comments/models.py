from django.db import models
from django.utils import timezone
from crowfounding import settings

from app_project.models import Project
from app_accounts.models import User


class Comment(models.Model):
    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ['-id']

    author = models.ForeignKey(User, related_name='author_comments', verbose_name='Автор',
                               null=True,
                               on_delete=models.PROTECT)
    project = models.ForeignKey(Project, null=True, verbose_name='Проект', on_delete=models.CASCADE)
    published_date = models.DateField(blank=True, null=True, default=timezone.now, verbose_name='Дата публикации')
    text = models.CharField('Текст', max_length=255)

    def __str__(self):
        return 'Комментарий №{} от {}'.format(self.id, self.author)

    def save(self, *args, **kwargs):
        self.published_date = timezone.now()
        super(Comment, self).save(*args, **kwargs)
