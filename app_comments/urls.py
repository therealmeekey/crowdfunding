from django.contrib.auth.decorators import login_required
from django.urls import path
from app_comments.views import *

app_name = 'comments'

urlpatterns = (
    path('create/<int:pk>/', login_required(create_comment), name='create'),
)
