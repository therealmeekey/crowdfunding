from django.shortcuts import redirect
from app_comments.forms import CommentForm
from app_project.models import Project
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def create_comment(request, pk):
    project = Project.objects.get(id=pk)
    if request.method == "POST":
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.project = project
            comment.save()
            return redirect('app_project:detail', slug=project.slug)
        else:
            print(comment_form.errors)
    else:
        comment_form = CommentForm()
    return redirect('app_project:detail', slug=project.slug)
