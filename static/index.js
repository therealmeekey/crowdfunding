M.AutoInit();
// $('.slider').slider({
//     full_width: true,
//     interval: 2000
// });

document.addEventListener('DOMContentLoaded', function () {
    var elems_sid = document.querySelectorAll('.sidenav');
    var instances_sid = M.Sidenav.init(elems_sid);
    var elems_category = document.getElementById('categories_filter');
    var instances_category = M.FormSelect.init(elems_category);
    var elems_region = document.getElementById('region_filter');
    var instances_region = M.FormSelect.init(elems_region);
    var elems_city = document.getElementById('city_filter');
    var instances_city = M.FormSelect.init(elems_city);
    var elems_tabs = document.querySelectorAll('.tabs');
    var instance_tabs = M.Tabs.init(elems_tabs);
    var elems = document.querySelectorAll('.slider');
    options = {
        height: 630,
    };
    var instances = M.Slider.init(elems, options);
    if (elems_region) {
        elems_region.onchange = () => {
            if (instances_region.getSelectedValues.length !== 0) {
                var region = instances_region.getSelectedValues();
            }
            else {
                var region = elems_region.value;
            }
            fetch('https://crowdfundingdiplom.herokuapp.com/region/get_city/', {
                method: 'POST',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json',
                    "Accept": "application/json",
                },
                body: JSON.stringify({
                    region: region
                })
            }).then((response) => {
                return response.json();
            }).then((data) => {
                var city_value = "<option value=\"\" disabled selected>Выберите город</option>";
                for (var city of data.city[0]) {
                    city_value += '<option value="' + city[0] + '">' + city[1] + '</option>';
                }
                document.getElementById("city_filter").innerHTML = city_value;
                instances_city = M.FormSelect.init(elems_city);
            });
        };
    }
});
