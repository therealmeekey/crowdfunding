from django.urls import path
from app_landing.views import *

app_name = 'landing'

urlpatterns = (
    path('', LandingView.as_view(), name='index'),
)
