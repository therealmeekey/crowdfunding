from django.views.generic import TemplateView
from app_category.models import Category
from app_project.models import Project


class LandingView(TemplateView):
    """
    Обработка страниц сайта
    """
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        utm_labels = {
            arg_name: arg_value for arg_name, arg_value in list(request.GET.items()) if arg_name.startswith('utm_')
        }
        request.session['utm_labels'] = utm_labels
        return super(LandingView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        projects_carusel = Project.objects.published().filter(important=True).order_by('-id')[:3][::-1]
        projects_last = Project.objects.published().order_by('-id')[:6][::-1]
        categories = Category.objects.all()
        context = {
            'projects_carusel': projects_carusel,
            'projects_last': projects_last,
            'categories': categories,
        }
        return context
