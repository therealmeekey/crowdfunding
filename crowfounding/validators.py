import re

from django.core import validators
from django.utils.deconstruct import deconstructible


@deconstructible
class YoutubeValidator(validators.RegexValidator):
    regex = r'^https?://(youtu\.be/[^/#]+|(www\.)?youtube\.com/.+)/?$'
    flags = re.IGNORECASE
    message = ('Идентификатор Youtube должен содержать корректный адрес '
               'канала, пользователя или видео')


@deconstructible
class VkValidator(validators.RegexValidator):
    regex = r'^https?://(www\.)?vk\.com/[a-z0-9._]+/?$'
    flags = re.IGNORECASE
    message = 'Идентификатор ВКонтакте должен содержать корректный адрес страницы'


@deconstructible
class FacebookValidator(validators.RegexValidator):
    regex = r'^https?://([^./]+\.)?(facebook.com|fb.com|fb.me)/[\w.=?%/-]+$'
    flags = re.IGNORECASE
    message = 'Идентификатор Facebook должен содержать корректный адрес страницы'


@deconstructible
class TwitterValidator(validators.RegexValidator):
    regex = r'^https?://(www\.)?twitter\.com/[a-z0-9_]+/?$'
    flags = re.IGNORECASE
    message = 'Идентификатор Twitter должен содержать корректный адрес страницы'


@deconstructible
class OkValidator(validators.RegexValidator):
    regex = r'^https?://(www\.)?ok\.ru/([^/?#]+|(profile|group)/[0-9]+)/?$'
    flags = re.IGNORECASE
    message = 'Идентификатор Одноклассники должен содержать корректный адрес страницы'


@deconstructible
class InstagramValidator(validators.RegexValidator):
    regex = r'^https?://(www\.instagram\.com|instagram\.com|instagr\.am)/[a-z0-9._]+/?$'
    flags = re.IGNORECASE
    message = 'Идентификатор Instagram должен содержать корректный адрес страницы'
