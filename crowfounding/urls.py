from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import path, include
from django.conf.urls.static import static
from crowfounding import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('app_accounts.urls')),
    path('', include('app_landing.urls')),
    path('app_project/', include('app_project.urls')),
    path('comments/', include('app_comments.urls')),
    path('region/', include('app_region.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('favicon.ico/', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'favicon.ico'))
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
