from django.urls import path
from app_region.views import *

app_name = 'region'

urlpatterns = (
    path('get_city/', get_city, name='get_city'),
    path('project_coord/', project_coord, name='project_coord'),
)
