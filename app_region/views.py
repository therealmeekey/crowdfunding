import csv
import json
import os
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from app_project.models import Project
from app_region.models import City, Region

DATA_PATH = os.path.join(os.path.dirname(__file__), 'data')


@csrf_exempt
def get_city(request):
    data = json.loads(request.body)
    if 'region' in data:
        citys = City.objects.filter(region__id__in=[int(i) for i in data['region']])
        if len(citys) == 0:
            citys = City.objects.filter(region__id=data['region'])
        city_list = []
        for c in citys:
            city_list.append([c.id, c.name])
        context = {'city': [city_list]}
        return HttpResponse(json.dumps(context),
                            content_type="application/json", status=200)


def create_region_city():
    file = open(DATA_PATH + '/city.csv', 'r')
    data = csv.reader(file)
    i = 0
    for row in data:
        if i != 0:
            type_region = row[4]
            region = row[5]
            city = row[9]
            coord_x = row[20]
            coord_y = row[21]
            if type_region == 'Респ':
                region = 'Республика %s' % (region,)
            elif type_region == 'край':
                region = '%s край' % (region,)
            elif type_region == 'обл':
                region = '%s область' % (region,)
            elif type_region == 'аобл':
                region = '%s автономная область' % (region,)
            elif type_region == 'г':
                city = region
                region = 'город %s' % (region,)
            elif type_region == 'АО' and region != 'Ханты-Мансийский Автономный округ - Югра':
                region = '%s автономный округ' % (region,)
            region, cregion = Region.objects.get_or_create(name=region)
            city, crcity = City.objects.get_or_create(name=city, region=region, coord_x=coord_x, coord_y=coord_y)
        i += 1
    return HttpResponse(status=200)


@csrf_exempt
def project_coord(request):
    # datas = json.loads(request.body)
    features_list = []
    data = {"type": "FeatureCollection",
            "features": features_list}
    color = ['islands#orangeCircleDotIconWithCaption', 'islands#redCircleDotIconWithCaption',
             'islands#darkBlueCircleDotIconWithCaption', 'islands#pinkCircleDotIconWithCaption',
             'islands#grayCircleDotIconWithCaption', 'islands#brownCircleDotIconWithCaption',
             'islands#yellowCircleDotIconWithCaption', 'islands#oliveCircleDotIconWithCaption',
             'islands#nightCircleDotIconWithCaption', 'islands#blueCircleDotIconWithCaption',
             'islands#darkGreenCircleDotIconWithCaption', 'islands#darkOrangeCircleDotIconWithCaption',
             'islands#blackCircleDotIconWithCaption', 'islands#lightBlueCircleDotIconWithCaption']
    projects = Project.objects.published()
    i = projects.count()
    for p in projects:
        if p.city.coord_y and p.city.coord_x:
            coord = []
            coord.append([float(p.city.coord_y), float(p.city.coord_x)])
            json_coord = {
                "type": "Feature",
                "id": i,
                "geometry":
                    {
                        "type": "Point",
                        "coordinates": coord[0]
                    },
                "properties":
                    {
                        "balloonContentHeader": "<font size=3><b><a target='_blank' href='https://yandex.ru'>Здесь может быть ваша ссылка</a></b></font>",
                        "balloonContentBody": "<p>Ваше имя: <input name='login'></p><p><em>Телефон в формате 2xxx-xxx:</em>  <input></p><p><input type='submit' value='Отправить'></p>",
                        "balloonContentFooter": "<font size=1>Информация предоставлена: </font> <strong>этим балуном</strong>",
                        "balloonContent": "<strong>Адрес: {}</strong>".format(p.title),
                        'clusterCaption': '{}'.format(p.pre_title),
                        'hintContent': "<strong>Адрес: {}</strong>".format(p.estimated_amount),
                    },
                # 'options':
                #     {
                #         "preset": color[inex_color],
                #     }
            }
            features_list.append(json_coord)
            i += 1
        else:
            pass
        # if i == 0:
        #     continue
    # inex_color += 1
    # if inex_color == 14:
    #     inex_color = 0
    return JsonResponse(data)
