from django.apps import AppConfig


class AppRegionConfig(AppConfig):
    name = 'app_region'
    verbose_name = 'Регионы и города'
