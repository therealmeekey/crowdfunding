from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from app_region.views import create_region_city
from app_region.models import Region, City
from functools import update_wrapper


class CityInline(admin.TabularInline):
    model = City
    insert_after = 'name'
    extra = 1

@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ['id']
    inlines = [CityInline]

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        urlpatterns = super().get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name

        urls = [
                   path('create_region_city/', wrap(self.create_region_city), name='%s_%s_create_region_city' % info),
               ] + urlpatterns

        return urls

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        # extra_context.update({'sync_state': NewsSync.load()})
        return super().changelist_view(request, extra_context=extra_context)

    def create_region_city(self, request):
        info = self.model._meta.app_label, self.model._meta.model_name
        create_region_city()
        redirect_url = reverse('admin:%s_%s_changelist' % info)
        return HttpResponseRedirect(redirect_url)
