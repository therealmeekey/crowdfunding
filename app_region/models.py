from django.db import models


class Region(models.Model):
    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'
        ordering = ['id']

    name = models.CharField('Название', max_length=255)

    def __str__(self):
        return self.name


class City(models.Model):
    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
        ordering = ['id']

    region = models.ForeignKey(Region, on_delete=models.CASCADE, verbose_name='Регион')
    name = models.CharField('Название', max_length=255)
    coord_x = models.DecimalField('Ширина', max_digits=9, decimal_places=6, blank=True, null=True)
    coord_y = models.DecimalField('Долгота', max_digits=9, decimal_places=6, blank=True, null=True)

    def __str__(self):
        return self.name
