import os
import sys
import django
from crowfounding.settings import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'crowfounding.settings')
django.setup()

import logging
import time
import datetime
import json
import csv
from django.http import HttpResponse
from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(BASE_DIR)
DATA_PATH = os.path.join(os.path.dirname(__file__), 'app_category', 'data')

